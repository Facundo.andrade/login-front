import { HttpClient, JsonpClientBackend } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiserviceService {

  [x: string]: any;
  url = 'http://avaluation.alwaysdata.net/miapi';
  //url = 'http://localhost:3000/miapi'

  constructor() { }

  async getAlumnos() {
    const info = await (await fetch(`${this.url}/getAlumnos`)).json()
    console.log(info)
    return info
  }

  async getDni(user: any) {
    const info = await (await fetch(`${this.url}/Login`,{
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      
      body: JSON.stringify(user)
    })).json()
    return info
  }

}
